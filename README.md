# CREA-forestry-and-wood_study-areas

The main motivation behind this simple shiny app is to make it easier to locate experimental sites (i.e. study areas) used by the Italian Research centre for forestry and wood (CREA) for its projects